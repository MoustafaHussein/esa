package edu.wiki.util;

import redis.clients.jedis.Jedis;

import java.io.*;
import java.nio.ByteBuffer;
import java.sql.*;

public class RedisLoader {
    private PreparedStatement preparedStatementTermJoinQuery;
    private String joinQuery = "select terms.term, terms.idf, idx.vector from terms join idx on terms.term = idx.term";
    private String strMaxConcept = "SELECT MAX(id) FROM article";
    private int maxConceptId = 1;

    private void initDB(Connection connection) throws ClassNotFoundException, SQLException, IOException {
        preparedStatementTermJoinQuery = connection.prepareStatement(joinQuery);
        preparedStatementTermJoinQuery.setFetchSize(1000000);

        ResultSet res = connection.createStatement().executeQuery(strMaxConcept);
        res.next();
        maxConceptId = res.getInt(1) + 1;
    }

    public RedisLoader(Connection connection) throws SQLException, IOException, ClassNotFoundException {
        initDB(connection);
    }

    public void startLoad() throws SQLException, IOException {
        Jedis jedis = new Jedis("localhost");
        preparedStatementTermJoinQuery.execute();
        ResultSet rs = preparedStatementTermJoinQuery.getResultSet();
        int cnt = 0;
        while (rs.next()) {
            //term, idf, vector
            writeToRedis(jedis, rs.getBytes("term"), rs.getFloat("idf"), rs.getBytes("vector"));
            cnt += 1;
            if (cnt % 100 == 0) {
                System.out.println("Number of rows done till now = " + cnt);
            }
        }
        System.out.println("Finished Loading all rows");
        System.out.println("saving the maxConceptId to maxConceptId-dummySubstring-maxConceptId as string");
        jedis.set("maxConceptId-dummySubstring-maxConceptId", "" + maxConceptId);
    }

    private void writeToRedis(Jedis jedis, byte[] queryWord, float idf, byte[] vector) throws IOException, SQLException {
        //query is for sure in the sql dump
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byteStream.write(ByteBuffer.allocate(4).putFloat(idf).array());
        byteStream.write(vector);
        byte[] row = byteStream.toByteArray();
        jedis.set(queryWord, row);
    }
}
