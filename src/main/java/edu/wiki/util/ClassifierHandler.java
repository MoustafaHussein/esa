package edu.wiki.util;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class ClassifierHandler {
    private Model model;
    private static final Logger LOGGER = Logger.getLogger(ClassifierHandler.class.getName());
    private static final Pattern COLON = Pattern.compile(":");

    public ClassifierHandler(String modelFilePath) throws IOException {
        model = Linear.loadModel(new File(modelFilePath));
    }

    public int yusuf(String line) {
        //line on the form id:value id:value id:value
        try {
            List<Feature> x = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(line, " \t\n");
            int nr_feature = model.getNrFeature();

            while (st.hasMoreTokens()) {
                String[] split = COLON.split(st.nextToken(), 2);

                int idx = Integer.parseInt(split[0]);
                double val = Double.parseDouble(split[1]);

                // feature indices larger than those in training are not used
                if (idx <= nr_feature) {
                    Feature node = new FeatureNode(idx, val);
                    x.add(node);
                }
            }

            if (model.getBias() >= 0) {
                Feature node = new FeatureNode(nr_feature + 1, model.getBias());
                x.add(node);
            }

            Feature[] nodes = new Feature[x.size()];
            nodes = x.toArray(nodes);
            return (int) Linear.predict(model, nodes);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.info("Error in yusuf " + sw.toString());
        }
        return -1;
    }

    public String yusufProbability(String line) {
        //line on the form id:value id:value id:value
        try {
            List<Feature> x = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(line, " \t\n");
            int nr_feature = model.getNrFeature();
            int nr_class = model.getNrClass();
            int[] labels = model.getLabels();

            while (st.hasMoreTokens()) {
                String[] split = COLON.split(st.nextToken(), 2);

                int idx = Integer.parseInt(split[0]);
                double val = Double.parseDouble(split[1]);

                // feature indices larger than those in training are not used
                if (idx <= nr_feature) {
                    Feature node = new FeatureNode(idx, val);
                    x.add(node);
                }
            }

            if (model.getBias() >= 0) {
                Feature node = new FeatureNode(nr_feature + 1, model.getBias());
                x.add(node);
            }

            Feature[] nodes = new Feature[x.size()];
            nodes = x.toArray(nodes);

            double[] prob_estimates = new double[nr_class];
            int predict_label = (int) Linear.predictProbability(model, nodes, prob_estimates);
            Map<Integer, Double> map = new HashMap<>();
            for (int j = 0; j < nr_class; j++)
                map.put(labels[j], prob_estimates[j]);
            return "" + predict_label + " " + map.get(predict_label).toString();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.info("Error in yusufProbability " + sw.toString());
        }
        return "-1";
    }
}
