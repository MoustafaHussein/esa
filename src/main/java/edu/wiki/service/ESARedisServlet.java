package edu.wiki.service;

import edu.wiki.demo.TestGeneralESAVectorsRedis;
import edu.wiki.util.ClassifierHandler;
import edu.wiki.util.RedisLoader;
import edu.wiki.util.StopWords;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ESARedisServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ServletContext context;
    private static GenericObjectPool<TestGeneralESAVectorsRedis> pool;
    private static final Logger LOGGER = Logger.getLogger(ESARedisServlet.class.getName());
    private ClassifierHandler classifierHandler;

    private static class Factory extends BasePooledObjectFactory<TestGeneralESAVectorsRedis> {

        @Override
        public TestGeneralESAVectorsRedis create() throws Exception {
            return new TestGeneralESAVectorsRedis();
        }

        @Override
        public PooledObject<TestGeneralESAVectorsRedis> wrap(TestGeneralESAVectorsRedis testGeneralESAVectorsRedis) {
            return new DefaultPooledObject<>(testGeneralESAVectorsRedis);
        }
    }


    public static void initDB() throws ClassNotFoundException, SQLException, IOException {
        // Load the JDBC driver
        String driverName = "com.mysql.jdbc.Driver"; // MySQL Connector
        Class.forName(driverName);

        // String url = "jdbc:mysql://" + "localhost" + "/" + "wiki2"; // a JDBC url
        String url = StopWords.SQL_URL;
        Connection connection = DriverManager.getConnection(url, "root", "123456");
        RedisLoader loader = new RedisLoader(connection);
        loader.startLoad();
        System.out.println("LOADING TO REDIS SUCCESSFUL");
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(1000);
        pool = new GenericObjectPool<>(new Factory(), config);
        System.out.println("GenericObjectPool Creation SUCCESSFUL");
    }


    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            initDB();
        } catch (ClassNotFoundException | SQLException | IOException e) {
            e.printStackTrace();
        }
        try {
            classifierHandler = new ClassifierHandler("/grad/Redis_ESA/shit2/train_2.txt.model");
            LOGGER.info("Classifier Model Loaded Successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
        context = config.getServletContext();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setHeader("Cache-Control", "no-cache");
        response.setCharacterEncoding("UTF-8");
        String task = request.getParameter("task");
        try {

            //redirect to home page if there is no task
            if (task == null) {
                response.setContentType("text/html");
                response.getWriter().append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><meta http-equiv=\"REFRESH\" content=\"0;url=").append(context.getInitParameter("server_path")).append("></head><body></body></html>");
                return;
            }

            //process compare request
            if (task.equals("vector")) {
                handleConceptVector(request, response);
            }
            if (task.equals("production")) {
                production(request, response, true);
            }
            if (task.equals("productionProbability")) {
                production(request, response, false);
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.info("SHIT IN SERVER" + sw.toString());
            this.reset(response, e.getMessage());
        }
    }

    private void reset(HttpServletResponse response, String error) throws IOException {
        response.reset();
        response.setContentType("application/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.setCharacterEncoding("UTF8");
        response.getWriter().append(error);
    }

    private void handleConceptVector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String source = request.getParameter("source");
        response.setContentType("text/html");
        if (source == null) {
            response.getWriter().append("");
            return;
        }
        String arr[] = source.split("\n");
        TestGeneralESAVectorsRedis solver = pool.borrowObject();
        for (String query : arr) {
            String temp = solver.getLine(query);
            if (temp == null) {
                LOGGER.log(Level.FINER, "NULL VALUE FOUND");
            }
            response.getWriter().append(temp).append("\n");
        }
        pool.returnObject(solver);
    }

    private void handleClassification(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String source = request.getParameter("source");
        response.setContentType("text/html");
        if (source == null) {
            response.getWriter().append("-1");
            return;
        }
        String text[] = source.split("\n");
        TestGeneralESAVectorsRedis solver = pool.borrowObject();
        for (String aText : text) {
            String temp = solver.getLine(aText);
            if (temp == null) {
                LOGGER.log(Level.FINER, "NULL VALUE FOUND");
                response.getWriter().append(temp.substring(temp.lastIndexOf("#") + 1)).append(":-1").append("\n");
            } else {
                // TODO: 6/20/17 handle for production
                //temp is on the form 1,2,3 id1:val1 id2:val #tweet_id
                //send to calcification substring first occurrence of space to last occurrence of space

                String vector;
                try {
                    vector = temp.substring(temp.indexOf(' ') + 1, temp.lastIndexOf(' '));
                } catch (Exception e) {
                    vector = "";
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    LOGGER.info("\nsetting vector to empty " + sw.toString());
                }
                response.getWriter().append(temp.substring(temp.lastIndexOf("#") + 1)).append(":").append("" + classifierHandler.yusuf(vector)).append("\n");
            }
        }
        pool.returnObject(solver);
    }

    private void newHandleMethodTest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String source = request.getParameter("source");
        response.setContentType("text/html");
        // TODO: 6/20/17 handle for production
        //temp is on the form 1,2,3 id1:val1 id2:val #tweet_id
        //send to calcification substring first occurrence of space to last occurrence of space

        String vector;
        try {
            vector = source.substring(source.indexOf(' ') + 1, source.lastIndexOf(' '));
        } catch (Exception e) {
            vector = "";
        }
        String id = source.substring(source.lastIndexOf("#") + 1);
        response.getWriter().append(id).append(":").append("" + classifierHandler.yusuf(vector)).append("\n");
    }

    private void production(HttpServletRequest request, HttpServletResponse response, boolean method) throws Exception {
        String tweetText = request.getParameter("source");
        response.setContentType("text/html");
        if (tweetText == null) {
            response.getWriter().append("-1");
            return;
        }
        TestGeneralESAVectorsRedis solver = null;
        try {
            solver = pool.borrowObject();
            String temp = solver.getVector(tweetText);
            if (temp == null) {
                LOGGER.log(Level.FINER, "NULL VALUE FOUND");
                response.getWriter().append("-1");
            } else {
                //temp is on the form id1:val1 id2:val
                if (method)
                    response.getWriter().append("" + classifierHandler.yusuf(temp));
                else {
                    response.getWriter().append(classifierHandler.yusufProbability(temp));
                }
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.info("error in production function " + sw.toString());
            response.getWriter().append("-1");
        } finally {
            if (solver != null)
                pool.returnObject(solver);
        }
    }
}
