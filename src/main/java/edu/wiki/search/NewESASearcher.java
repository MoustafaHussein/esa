package edu.wiki.search;

import edu.wiki.api.concept.IConceptVector;
import edu.wiki.concept.TroveConceptVector;
import edu.wiki.index.WikipediaAnalyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Performs search on the index located in database.
 *
 * @author Cagatay Calli <ccalli@gmail.com>
 */
public class NewESASearcher {
    private Connection connection;
    private PreparedStatement pstmtQuery;
    private PreparedStatement pstmtIdfQuery;
    private WikipediaAnalyzer analyzer;

    private String strTermQuery = "SELECT t.vector FROM idx t WHERE t.term = ?";
    private String strIdfQuery = "SELECT t.idf FROM terms t WHERE t.term = ?";
    private HashMap<String, Integer> freqMap = new HashMap<>(30);
    private HashMap<String, Double> tfidfMap = new HashMap<>(30);
    private HashMap<String, Float> idfMap = new HashMap<>(30);
    private HashMap<Integer, Double> conceptMap = new HashMap<>();
    private ArrayList<String> termList = new ArrayList<>(30);


    public void initDB() throws ClassNotFoundException, SQLException, IOException {
        // Load the JDBC driver
        String driverName = "com.mysql.jdbc.Driver"; // MySQL Connector
        Class.forName(driverName);

        // read DB config
        InputStream is = ESASearcher.class.getResourceAsStream("/db.conf");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String serverName = br.readLine();
        String mydatabase = br.readLine();
        String username = br.readLine();
        String password = br.readLine();
        br.close();

        // Create a connection to the database
        String url = "jdbc:mysql://" + serverName + "/" + mydatabase; // a JDBC url
        connection = DriverManager.getConnection(url, username, password);

        pstmtQuery = connection.prepareStatement(strTermQuery);
        pstmtQuery.setFetchSize(1);

        pstmtIdfQuery = connection.prepareStatement(strIdfQuery);
        pstmtIdfQuery.setFetchSize(1);
    }

    private void clean() {
        freqMap.clear();
        tfidfMap.clear();
        idfMap.clear();
        termList.clear();
        conceptMap.clear();
    }

    public NewESASearcher() throws ClassNotFoundException, SQLException, IOException {
        initDB();
        analyzer = new WikipediaAnalyzer();
    }

    @Override
    protected void finalize() throws Throwable {
        connection.close();
        super.finalize();
    }

    /**
     * Retrieves full vector for regular features
     *
     * @param query
     * @return Returns concept vector results exist, otherwise null
     * @throws IOException
     * @throws SQLException
     */
    public IConceptVector getConceptVector(String query) throws IOException, SQLException {
        String strTerm;
        ResultSet rs;
        double score, vdouble, tf, vsum;
        int vint, doc, numTerms = 0, plen;
        TokenStream ts = analyzer.tokenStream("contents", new StringReader(query));
        ByteArrayInputStream bais;
        DataInputStream dis;

        this.clean();
        ts.reset();

        while (ts.incrementToken()) {

            TermAttribute t = ts.getAttribute(TermAttribute.class);
            strTerm = t.term();

            // record term IDF
            if (!idfMap.containsKey(strTerm)) {
                pstmtIdfQuery.setBytes(1, strTerm.getBytes("UTF-8"));
                pstmtIdfQuery.execute();

                rs = pstmtIdfQuery.getResultSet();
                if (rs.next()) {
                    idfMap.put(strTerm, rs.getFloat(1));
                }
            }

            // records term counts for TF
            if (freqMap.containsKey(strTerm)) {
                vint = freqMap.get(strTerm);
                freqMap.put(strTerm, vint + 1);
            } else {
                freqMap.put(strTerm, 1);
            }

            termList.add(strTerm);

            numTerms++;

        }

        ts.end();
        ts.close();

        if (numTerms == 0) {
            return null;
        }

        // calculate TF-IDF vector (normalized)
        vsum = 0;
        for (String tk : idfMap.keySet()) {
            tf = 1.0 + Math.log(freqMap.get(tk));
            vdouble = (idfMap.get(tk) * tf);
            tfidfMap.put(tk, vdouble);
            vsum += vdouble * vdouble;
        }
        vsum = Math.sqrt(vsum);


        // comment this out for canceling query normalization
        for (String tk : idfMap.keySet()) {
            vdouble = tfidfMap.get(tk);
            tfidfMap.put(tk, vdouble / vsum);
        }

        score = 0;
        for (String tk : termList) {

            pstmtQuery.setBytes(1, tk.getBytes("UTF-8"));
            pstmtQuery.execute();

            rs = pstmtQuery.getResultSet();

            if (rs.next()) {
                bais = new ByteArrayInputStream(rs.getBytes(1));
                dis = new DataInputStream(bais);

                /**
                 * 4 bytes: int - length of array
                 * 4 byte (doc) - 8 byte (tfidf) pairs
                 */

                plen = dis.readInt();
                for (int k = 0; k < plen; k++) {
                    doc = dis.readInt();
                    score = dis.readFloat();
                    conceptMap.put(doc, conceptMap.getOrDefault(doc, 0.0) + (score * tfidfMap.get(tk)));
                }

                bais.close();
                dis.close();
            }

        }

        // no result
        if (score == 0) {
            return null;
        }
        HashMap<Integer, Double> tempMap = new HashMap<>();

        for (HashMap.Entry<Integer, Double> entry : conceptMap.entrySet()) {
            if (entry.getValue() != 0)
                tempMap.put(entry.getKey(), entry.getValue());
        }

        IConceptVector newCv = new TroveConceptVector(tempMap.size());
        for (HashMap.Entry<Integer, Double> entry : conceptMap.entrySet()) {
            newCv.set(entry.getKey(), entry.getValue() / numTerms);
        }

        return newCv;
    }

}
