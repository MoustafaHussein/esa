package edu.wiki.search;

import edu.wiki.api.concept.IConceptVector;
import edu.wiki.concept.TroveConceptVector;
import edu.wiki.index.WikipediaAnalyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * Performs search on the index located in database.
 *
 * @author Cagatay Calli <ccalli@gmail.com>
 */
public class RedisESASearcher {

    private WikipediaAnalyzer analyzer;
    private Jedis jedis;
    private HashMap<String, Integer> freqMap = new HashMap<>(30);
    private HashMap<String, Double> tfidfMap = new HashMap<>(30);
    private HashMap<String, Float> idfMap = new HashMap<>(30);
    private HashMap<Integer, Double> conceptMap = new HashMap<>();
    private ArrayList<String> termList = new ArrayList<>(30);


    private void initDB() throws ClassNotFoundException, SQLException, IOException {
        jedis = new Jedis();
    }

    private void clean() {
        freqMap.clear();
        tfidfMap.clear();
        idfMap.clear();
        termList.clear();
        conceptMap.clear();
    }

    public RedisESASearcher() throws ClassNotFoundException, SQLException, IOException {
        initDB();
        analyzer = new WikipediaAnalyzer();
    }

    @Override
    protected void finalize() throws Throwable {
        jedis.close();
        super.finalize();
    }

    private Hashtable<String, byte[]> readFromRedis(byte[] queryWord) throws UnsupportedEncodingException {
        Hashtable<String, byte[]> ret = new Hashtable<>();
        ret.put("word", queryWord);
        if (jedis.exists(queryWord)) {
            byte[] value = jedis.get(queryWord);
            ret.put("idf", Arrays.copyOfRange(value, 0, 4));
            if (value.length > 4) {
                ret.put("pairs", Arrays.copyOfRange(value, 4, value.length));
            }
        }
        return ret;
    }

    /**
     * Retrieves full vector for regular features
     *
     * @param query
     * @return Returns concept vector results exist, otherwise null
     * @throws IOException
     * @throws SQLException
     */
    public IConceptVector getConceptVector(String query) throws IOException, SQLException {
        String strTerm;
        int numTerms = 0, doc, plen;
        double score, vdouble, tf, vsum;

        TokenStream tokenStream = analyzer.tokenStream("contents", new StringReader(query));
        this.clean();
        tokenStream.reset();

        while (tokenStream.incrementToken()) {
            TermAttribute t = tokenStream.getAttribute(TermAttribute.class);
            strTerm = t.term();
            // record term IDF
            if (!idfMap.containsKey(strTerm)) {
                Hashtable<String, byte[]> data = readFromRedis(strTerm.getBytes("UTF-8"));
                if (data.containsKey("idf")) {
                    idfMap.put(strTerm, ByteBuffer.wrap(data.get("idf")).getFloat());
                }
            }
            // records term counts for TF
            freqMap.put(strTerm, freqMap.getOrDefault(strTerm, 0) + 1);
            termList.add(strTerm);
            numTerms++;
        }

        tokenStream.end();
        tokenStream.close();

        if (numTerms == 0) {
            return null;
        }

        // calculate TF-IDF vector (normalized)
        vsum = 0;
        for (String tk : idfMap.keySet()) {
            tf = 1.0 + Math.log(freqMap.get(tk));
            vdouble = (idfMap.get(tk) * tf);
            tfidfMap.put(tk, vdouble);
            vsum += vdouble * vdouble;
        }
        vsum = Math.sqrt(vsum);

        // comment this out for canceling query normalization
        for (String tk : idfMap.keySet()) {
            vdouble = tfidfMap.get(tk);
            tfidfMap.put(tk, vdouble / vsum);
        }

        score = 0;
        for (String tk : termList) {
            Hashtable<String, byte[]> data = readFromRedis(tk.getBytes("UTF-8"));
            if (data.containsKey("pairs")) {
                final ByteArrayInputStream bais = new ByteArrayInputStream(data.get("pairs"));
                final DataInputStream dis = new DataInputStream(bais);
                plen = dis.readInt();
                for (int k = 0; k < plen; k++) {
                    doc = dis.readInt();
                    score = dis.readFloat();
                    conceptMap.put(doc, conceptMap.getOrDefault(doc, 0.0) + (score * tfidfMap.get(tk)));
                }
                bais.close();
                dis.close();
            }
        }

        // no result
        if (score == 0) {
            return null;
        }
        HashMap<Integer, Double> tempMap = new HashMap<>();

        for (HashMap.Entry<Integer, Double> entry : conceptMap.entrySet()) {
            if (entry.getValue() != 0)
                tempMap.put(entry.getKey(), entry.getValue());
        }

        IConceptVector newCv = new TroveConceptVector(tempMap.size());
        for (HashMap.Entry<Integer, Double> entry : conceptMap.entrySet()) {
            newCv.set(entry.getKey(), entry.getValue() / numTerms);
        }

        return newCv;
    }
}