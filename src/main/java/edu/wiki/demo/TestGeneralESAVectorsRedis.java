package edu.wiki.demo;

import edu.wiki.api.concept.IConceptIterator;
import edu.wiki.api.concept.IConceptVector;
import edu.wiki.search.RedisESASearcher;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Logger;

public class TestGeneralESAVectorsRedis {

    private static final Logger LOGGER = Logger.getLogger(TestGeneralESAVectorsRedis.class.getName());
    private RedisESASearcher searcher;

    public TestGeneralESAVectorsRedis() throws SQLException, IOException, ClassNotFoundException {
        searcher = new RedisESASearcher();
    }

    /**
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public String getVector(String text) throws ClassNotFoundException, SQLException, IOException {
        IConceptVector cvBase = searcher.getConceptVector(text);
        if (cvBase == null) {
            LOGGER.info("empty concept vector => " + text);
            return "";
        }

        IConceptIterator it = cvBase.iterator();

        TreeMap<Integer, Double> tree = new TreeMap<>();
        while (it.next()) {
            tree.put(it.getId(), it.getValue());
        }

        String ret = "";
        for (Map.Entry<Integer, Double> entry : tree.entrySet()) {
            ret += " " + entry.getKey() + ":" + entry.getValue();
        }

        return ret;
    }

    public String getLine(String line) {
        try {
            String arr[] = line.split("\\s");
            String s = arr[1];
            for (int i = 2; i < arr.length - 1; i++) {
                s += " " + arr[i];
            }
            String vector = getVector(s);
            return (arr[0].substring(0, arr[0].length() - 1) + vector + " #" + arr[arr.length - 1]);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.info("THIS IS THE FUCKING STACK TRACE" + sw.toString());
            return null;
        }
    }

    public void start(String inputFile, String outputFile) throws IOException, SQLException, ClassNotFoundException {
        PrintWriter writer = new PrintWriter(outputFile);
        Scanner input = new Scanner(new File(inputFile));
        int cnt = 0;
        while (input.hasNextLine()) {
            String line = input.nextLine();
            String res = getLine(line);
            writer.println(res);
            cnt++;
            if (cnt % 500 == 0)
                LOGGER.info(cnt + " lines are done");
        }
        writer.close();
        input.close();
    }

    public static void main(String args[]) throws IOException, SQLException, ClassNotFoundException {
        String message = "\n\tStart at TestGeneralESAVectorsRedis main function\n";
        message += "\tThis jar take two files and parameter to decide what database to use\n";
        message += "\tIf third parameter is \"redis\" will use the redis database\n";
        message += "\tThis jar input file must be with format\n\t\t\"label, tweet text #tweed_id\"";
        LOGGER.info(message);
        if (args != null && args.length == 3) {
            if (args[2].contains("redis")) {
                LOGGER.info("USING THE REDIS SEARCHER");
                TestGeneralESAVectorsRedis test = new TestGeneralESAVectorsRedis();
                LOGGER.info("source file " + args[0] + " " + "destination file " + args[1]);
                test.start(args[0], args[1]);
            } else {
                LOGGER.info("USING THE SQL SEARCHER");
                TestGeneralESAVectors test = new TestGeneralESAVectors();
                LOGGER.info("source file " + args[0] + " " + "destination file " + args[1]);
                test.start(args[0], args[1]);
            }
        } else {
            LOGGER.info("wrong number of args -> system exit with -1");
            System.exit(-1);
        }
    }
}
