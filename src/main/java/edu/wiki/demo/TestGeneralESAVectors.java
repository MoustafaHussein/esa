package edu.wiki.demo;

import edu.wiki.api.concept.IConceptIterator;
import edu.wiki.api.concept.IConceptVector;
import edu.wiki.search.ESASearcher;
import edu.wiki.util.StopWords;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Logger;

public class TestGeneralESAVectors {

    static Connection connection;
    static Statement stmtQuery;
    private ESASearcher searcher;
    private static final Logger LOGGER = Logger.getLogger(TestGeneralESAVectors.class.getName());
    private static boolean isDone = false;

    public static void initDB() throws ClassNotFoundException, SQLException, IOException {
        // Load the JDBC driver
        String driverName = "com.mysql.jdbc.Driver"; // MySQL Connector
        Class.forName(driverName);

//        // read DB config
//        InputStream is = ESASearcher.class.getResourceAsStream("/config/db.conf");
//        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//        String serverName = br.readLine();
//        String mydatabase = br.readLine();
//        String username = br.readLine();
//        String password = "123456";
//        br.close();

        // Create a connection to the database
//        String url = "jdbc:mysql://" + serverName + "/" + mydatabase; // a JDBC url

        //String url = "jdbc:mysql://" + "localhost" + "/" + "wiki2"; // a JDBC url
        String url = StopWords.SQL_URL;
        connection = DriverManager.getConnection(url, "root", "123456");
        stmtQuery = connection.createStatement();
        stmtQuery.setFetchSize(100);
    }

    /**
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public String getVector(String text) throws ClassNotFoundException, SQLException, IOException {
        if (!isDone) {
            searcher = new ESASearcher();
            initDB();
            isDone = true;
        }
        int limit = 500000; //set limit to big value to get all concepts
        IConceptVector cvBase = searcher.getConceptVector(text);
        IConceptVector cvNormal = searcher.getNormalVector(cvBase, limit);
        if (cvNormal == null) {
            LOGGER.info("empty concept vector => " + text);
            return "";
        }

        IConceptIterator it = cvNormal.orderedIterator();

        int count = 0;
        TreeMap<Integer, Double> tree = new TreeMap<>();
        while (it.next() && count < limit) {
            tree.put(it.getId(), it.getValue());
            count++;
        }

        String ret = "";
        for (Map.Entry<Integer, Double> entry : tree.entrySet()) {
            ret += " " + entry.getKey() + ":" + entry.getValue();
        }

        return ret;
    }

    public String getLine(String line) throws SQLException, IOException, ClassNotFoundException {
        String arr[] = line.split("\\s");
        String s = arr[1];
        for (int i = 2; i < arr.length - 1; i++) {
            s += " " + arr[i];
        }
        String vector = getVector(s);
        return (arr[0].substring(0, arr[0].length() - 1) + vector + " " + arr[arr.length - 1]);
    }

    public void start(String inputFile, String outputFile) throws IOException, SQLException, ClassNotFoundException {
        PrintWriter writer = new PrintWriter(outputFile);
        Scanner input = new Scanner(new File(inputFile));
        int cnt = 0;
        while (input.hasNextLine()) {
            String line = input.nextLine();
            String res = getLine(line);
            writer.println(res);
            cnt++;
            if (cnt % 500 == 0)
                LOGGER.info(cnt + " lines are done");
        }
        writer.close();
        input.close();
    }

    public static void main(String args[]) throws IOException, SQLException, ClassNotFoundException {
        LOGGER.info("start at TestGeneralESAVectors main function");
        if (args != null && args.length == 2) {
            TestGeneralESAVectors test = new TestGeneralESAVectors();
            LOGGER.info("source file " + args[0] + " " + "destination file " + args[1]);
            test.start(args[0], args[1]);
        } else {
            LOGGER.info("wrong number of args -> system exit with -1");
            System.exit(-1);
        }
    }
}
