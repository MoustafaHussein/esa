package edu.wiki.demo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public Main() throws SQLException, IOException, ClassNotFoundException {

    }

    public static void main(String args[]) throws IOException, SQLException, ClassNotFoundException {
        TestGeneralESAVectors x=new TestGeneralESAVectors();
        System.out.println(x.getLine("1, Proud to make this one of my final actions as President. America is a nation of second chances, and 1,715 people deserved that shot #122"));
//        String message = "\n\tStart at TestGeneralESAVectorsRedis main function\n";
//        message += "\tThis jar take two files and parameter to decide what database to use\n";
//        message += "\tIf third parameter is \"redis\" will use the redis database\n";
//        message += "\tThis jar input file must be with format\n\t\t\"label, tweet text #tweed_id\"";
//        LOGGER.info(message);
//        LOGGER.info("start at Main main function");
//        if (args != null && args.length == 3) {
//            LOGGER.info("source file " + args[0] + " " + "destination file " + args[1]);
//            if (args[2].equals("redis")) {
//                LOGGER.info("This is Redis");
//                TestGeneralESAVectorsRedis test = new TestGeneralESAVectorsRedis();
//                test.start(args[0], args[1]);
//            } else {
//                LOGGER.info("This is SQL");
//                TestGeneralESAVectors test = new TestGeneralESAVectors();
//                test.start(args[0], args[1]);
//            }
//        } else {
//            LOGGER.info("wrong number of args -> system exit with -1");
//            System.exit(-1);
//        }
    }
}
